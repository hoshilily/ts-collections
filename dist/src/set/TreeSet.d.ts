import { ISet } from "./ISet";
import { AbstractCollection } from "../core/AbstractCollection";
import { ICollection } from "../core/ICollection";
import { Constructor } from "../core/Constructor";
export declare class TreeSet<T> extends AbstractCollection<T> implements ISet<T> {
    private readonly comparator;
    private tree;
    constructor(comparator?: (v1: T, v2: T) => number);
    add(item: T): boolean;
    clear(): void;
    contains(item: T): boolean;
    isEmpty(): boolean;
    remove(item: T): boolean;
    size(): number;
    toArray(): T[];
    transform<U extends ICollection<T>>(Collection: Constructor<U>, comparator?: (v1: T, v2: T) => number): U;
}
